<?php 
namespace common {
	function beginWith ($s1, $s2) {
		return  (substr($s1, 0, strlen($s2))  ===  $s2)? 1 : 0;
	}

	function endWith ($s1, $s2) {
		return  (substr($s1, -strlen($s2))  ===  $s2)? 1 : 0;
	}	
}
?>
