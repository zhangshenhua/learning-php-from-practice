<html>
	<head>
		<meta charset="utf-8">
	</head>

	
	<body>
		<?php  
			if ($_GET['name']) {
				echo ('欢迎' . $_GET['name']);  
			}
		?>	
		
		<form action="13.php" method="get">
			<input name="name" type="text"  />
			<input type="submit" value="试试！"/>
		</form>
		
		这是第<?php echo ++$_SESSION['acc']; ?>次访问	
		
		
		<hr/>
		<p>第12关 字符串和unicode，实现beginWith和endWith </p>
		

		<form action="13.php" method="get">
			<input type="submit" value="beginWith"/>
			(<input name="s1" type="text" value="<?php echo $_GET['s1']?>" />, <input name="s2" type="text" value="<?php echo $_GET['s2']?>" />)
			=
			<input type="text" value="<?php echo common\beginWith($_GET['s1'], $_GET['s2']); ?>" />
		</form>
		
		<form action="13.php" method="get">
			<input type="submit" value="endWith"/>
			(<input name="s1" type="text" value="<?php echo $_GET['s1']?>" />, <input name="s2" type="text" value="<?php echo $_GET['s2']?>" />)
			=
			<input type="text" value="<?php echo common\endWith($_GET['s1'], $_GET['s2']); ?>" />
		</form>
		
		<hr/>
		<p>第13关 include和require， 实现前后端分离 </p>
		先要弄懂【include和require】与【实现前后端分离】逻辑上的蕴含关系。
		从查看官方文档开始：<br/>
		<a href='https://www.php.net/manual/zh/function.include.php'>include</a><br/>
		<a href='https://www.php.net/manual/zh/function.require.php'>require</a><br/>
		那么怎么叫做前后端分离离呢？前后端分离的要点是什么呢？<br/>
		请教王老师<br/>
		<pre>
王霄池 19:40:43
文件分离

面壁者 19:41:52
把定义归到一个文件里，不要散落一地，是这意思吗？

王霄池 19:45:25
前后端的文件分离，前端一个，后端一个

面壁者 19:46:53
怎么算达到目的呢

面壁者 19:47:14
我有点分不清前后

王霄池 19:48:21
这样就可以前后端两个人合作开发了

面壁者 19:49:07
目的是一个人管一个文件是吗？

王霄池 19:50:17
对

面壁者 19:50:46
谢谢王老师

		</pre>
		<hr/>
		代码清单：
<pre>
common.php
------------------------------------------------------------------------
<?php 
echo  htmlspecialchars(shell_exec('cat common.php')) ;
?>
------------------------------------------------------------------------


13.php
--------------------------------------------------------------------------
<?php 
echo  htmlspecialchars(shell_exec('cat 13.php')) ;
?>	
--------------------------------------------------------------------------
</pre>
	</body>
</html>