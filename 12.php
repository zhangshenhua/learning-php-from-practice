<?php 
/*
			# 通过实践学PHP-提纲

我会布置任务。如果完成的有困难或者超过两个小时，可以问我。

| 序号 | 任务                                  | 通过标准                               |            
| ---- | ------------------------------------- | -------------------------------------- |            
| 1    | 安装PHP（提示，windows可以安装xampp） | 可以访问 <?php phpinfo()               |            
| 2    | 基本HTML                              | 模仿一个界面                           |            
| 3    | 基本CSS                               | 美化一个界面                           |            
| 4    | 欢迎界面和$_GET                       | 欢迎语中带有用户的名字                 |            
| 5    | $_SESSION记录访问次数                 | 正确显示访问次数                       |            
| 6    | 如果                                  | 理解if语句                             |            
| 7    | 当山峰没有棱角的时候                  | 理解while语句                          |            
| 8    | 表达式                                | 理解运算符                             |            
| 9    | 函数                                  | 理解函数调用                           |            
| 10   | 命名空间和类                          | 理解命名空间和类                       |            
| 11   | 数组和leetcode习题                    | 做几道简单习题                         |            
| 12   | 字符串和unicode                       | 实现beginWith和endWith                 |   <--------------         
| 13   | include和require                      | 实现前后端分离                         |            
| 14   | （开始实践）模仿知乎-登录             | 登录界面和功能                         |            
| 15   | MySQL初步                             | 设计用户表，条目（问题、答案、评论）表 |            
| 16   | 登录-数据库                           | 带有数据库的登录和注册                 |            
| 17   | 提问                                  | 提问                                   |            
| 18   | 回答问题                              | 回答问题                               |            
| 19   | 评论                                  | 评论                                   |            
| 20   | 赞和transaction                       | 赞                                     |            
| 21   | 我的主页和MySQL索引                   | 我的主页                               |            
| 22   | 框架、第三方库、composer              | 飞翔吧                                 |   

*/

session_start() ?>
<html>
	<head>
		<meta charset="utf-8">
	</head>

	
	<body>
		<pre>
        


		</pre>
		
		
		<?php  
			if ($_GET['name']) {
				echo ('欢迎' . $_GET['name']);  
			}
		?>	
		
		<form action="12.php" method="get">
			<input name="name" type="text"  />
			<input type="submit" value="试试！"/>
		</form>
		
		这是第<?php echo ++$_SESSION['acc']; ?>次访问	
		
		
		<hr/>
		<p>第12关 字符串和unicode，实现beginWith和endWith </p>
		

		<form action="12.php" method="get">
			<input type="submit" value="beginWith"/>
			(<input name="s1" type="text" value="<?php echo $_GET['s1']?>" />, <input name="s2" type="text" value="<?php echo $_GET['s2']?>" />)
			=
			<input type="text" value="<?php echo beginWith($_GET['s1'], $_GET['s2']); ?>" />
		</form>
		
		<form action="12.php" method="get">
			<input type="submit" value="endWith"/>
			(<input name="s1" type="text" value="<?php echo $_GET['s1']?>" />, <input name="s2" type="text" value="<?php echo $_GET['s2']?>" />)
			=
			<input type="text" value="<?php echo endWith($_GET['s1'], $_GET['s2']); ?>" />
		</form>
		
		
		<?php 
			function beginWith ($s1, $s2) {
				return  (substr($s1, 0, strlen($s2))  ===  $s2)? 1 : 0;
			}
			
			function endWith ($s1, $s2) {
				return  (substr($s1, -strlen($s2))  ===  $s2)? 1 : 0;
			}			
		?>
		
		
		<hr/>
		全部代码：
		<pre>
			<?php 
				echo  htmlspecialchars(shell_exec('cat ' . __FILE__)) ;
			?>		
		</pre>
	</body>
</html>